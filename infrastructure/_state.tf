terraform {
  backend "s3" {
    profile = "mani_payuk"
    bucket  = "hello-world-tfstate.manifesto.co.uk"
    key     = "state.tfstate"
    region  = "eu-west-1"
  }
}
