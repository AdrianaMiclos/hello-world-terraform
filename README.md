
This repository contains the terraform setup for creating an AWS API Gateway and an AWS Lambda function together with a "hello-world" code for the lambda function.

## Getting started

You will need [terraform](https://learn.hashicorp.com/terraform/getting-started/install.html#install-terraform) installed.

Next you will need to create a local [AWS profile](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-profiles.html) called `mani_payuk` in `~/.aws` and add your credentials.

## How to run 
1. Set up the correct AWS profile
``` export AWS_PROFILE=mani_payuk```

2. Build and Deploy the code. The following command creates a zip of the code and uploads it in S3.
```npm run deploy```
 
3. Create the infrastructure:  
    * Go inside the infrastructure directory. All the next terraform commands will be run from this directory.
        ```cd infrastructure```

    * Run the following commands: 
        * ``terraform init`` -  gets the code for the configured provider and updates the local configuration that sits inside the `.terraform` directory
        * ``terraform get`` - loads/updates the terraform modules 
        * ``terraform plan`` - displays an execution plan that presents the stack changes compared to the current terraform state. The state is stored in a S3 bucket. Check `_state.tf` file for more details. 
        * ``teraform apply`` - applies our execution plan and builds the stack
 
 
